import random
from loguru import logger
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.units import cm
import math



class QuestionGeneator:

    questions = None
    answers = None

    def generateQuestions(self, type):
        questions = []
        answers = []
        for i in range(questionCount):
            [question, answer] = self.getQuestion(type)
            logger.debug(question)
            questions.append(question)
            answers.append(answer)

        logger.debug(len(questions))
        logger.debug(questions)
        self.questions  = questions
        self.answers  = answers
        return questions

    def getQuestion(self, type):
        if (type == 1): # 九九乘法
            x = random.randint(minX, maxX)
            y = random.randint(minY, maxY)
            question = "%d x %d = "%(x, y)
            answer = "%d x %d = %d"%(x, y, x*y)
        elif (type == 2): # 除法
            x = random.randint(minX, maxX)
            y = random.randint(x+1, x*10)
            question = "%d / %d = "%(y, x)
            answer = "%d / %d = %d..%d"%(y, x, int(y/x), y%x)
        return [question, answer]

    def printToPdf(self, filename, questions):
        canvas = Canvas(filename)
        canvas.setFont("Times-Roman", 22)
        # drawString 參數1：左邊距離，單位 1/72 吋，72 即為 1 吋
        # drawString 參數1：下邊距離，單位 1/72 吋，72 即為 1 吋
        # canvas.drawString(1 * cm, 1 * cm, "Hello, World")
        # canvas.save()
        i = 0
        for j in range(column):
            # question loop k
            for k in range(questionPerColumn):
                if (i == questionCount):
                    break
                left = marginLeft + widthPerQuestion * j
                bottom = marginBottom + (questionPerColumn - k) * heightPerQuestion
                question = questions[i]
                i += 1
                # logger.debug("left: %s"%(left))
                # logger.debug("bottom: %s"%(bottom))
                # logger.debug("question: %s"%(question))
                canvas.drawString(left * cm, bottom * cm, question)

        canvas.save()

    def start(self, type):
        for i in range(paperCount):
            self.generateQuestions(type)
            filename = "%s_%04d.pdf"%(filenamePre, i+1)
            self.printToPdf(filename, self.questions)
            filename = "answer_%s_%04d.pdf"%(filenamePre, i+1)
            self.printToPdf(filename, self.answers)
            

from type2 import *

questionGeneator = QuestionGeneator()
questionGeneator.start(2)
# questions = questionGeneator.generateQuestion()
# questionGeneator.printToPdf()




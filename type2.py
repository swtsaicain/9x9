import math
from loguru import logger

## 除法設定檔

# 最小 x
minX = 2
# 最大 x
maxX = 9
# 最小 y
minY = 2
# 最大 y
maxY = 9
# 題目數
questionCount = 80
# 試卷張數
paperCount = 10
# 試卷檔名
filenamePre = "除法"

# pdf 輸出相關設定
marginTop = 3
marginBottom = 0
marginLeft = 1.5
marginRight = 0
column = 4 # 行數
paperWidth = 21 # A4 紙寬度
paperHeight = 29.7 # A4紙高度

# 計算結果，不需修改
questionPerColumn = math.ceil(questionCount / column) # 每行問題數量
width = paperWidth - marginLeft - marginRight
height = paperHeight - marginTop - marginBottom
widthPerQuestion = width / column
heightPerQuestion = height / questionPerColumn
logger.debug("width: %s"%(width))
logger.debug("height: %s"%(height))
logger.debug("widthPerQuestion: %s"%(widthPerQuestion))
logger.debug("heightPerQuestion: %s"%(heightPerQuestion))